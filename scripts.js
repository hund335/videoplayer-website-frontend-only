function getHeader(){
    
    var req = new XMLHttpRequest();
    req.onreadystatechange = function(){
        if(req.readyState == 4){
            document.getElementById('outerheader').innerHTML = req.responseText;
        }
    };
    req.open("GET", "api/header.html", true);
    req.send();
    
}

function showMenu(){
    if(document.getElementById('headersidemenu').style.display == "none"){
        document.getElementById('headersidemenu').style.display = "block";
    }else{
        document.getElementById('headersidemenu').style.display = "none";
    }
}


function indexPage(div, page){
    var req = new XMLHttpRequest();
    req.onreadystatechange = function(){
        
        if(req.readyState == 4){
            document.getElementById('ajaxpage').innerHTML = req.responseText;
            
            for(var i = 1; i <= 3; i++){
                document.getElementById('info' + i).style.backgroundColor = "darkgrey";
                document.getElementById('info' + i).style.border = "darkgrey solid";
            }   
            
            document.getElementById(div).style.backgroundColor = "#a03030";
            document.getElementById(div).style.border = "#a03030 solid";
        }
    };
    req.open("GET", "api/" + page + ".html", true);
    req.send();
    
}

function nextVideo(){
    if(document.getElementById('image1').getAttribute("src") == "videos/1/1.png"){
        document.getElementById('image1').src = "videos/3/3.png";
        document.getElementById('image2').src = "videos/1/1.png";
        document.getElementById('image3').src = "videos/2/2.png";
        callVideo('1');
        return;
        
    }else if(document.getElementById('image1').getAttribute("src") == "videos/3/3.png"){       
        document.getElementById('image1').src = "videos/2/2.png";
        document.getElementById('image2').src = "videos/3/3.png";
        document.getElementById('image3').src = "videos/1/1.png";
        callVideo('3');
        return;
        
    }else{ 
        document.getElementById('image1').src = "videos/1/1.png";
        document.getElementById('image2').src = "videos/2/2.png";
        document.getElementById('image3').src = "videos/3/3.png";
        callVideo('2');
        return;
    }
}

function backVideo(){
    if(document.getElementById('image1').getAttribute("src") == "videos/1/1.png"){
        document.getElementById('image1').src = "videos/2/2.png";
        document.getElementById('image2').src = "videos/3/3.png";
        document.getElementById('image3').src = "videos/1/1.png";
        callVideo('3');
        return;
        
    }else if(document.getElementById('image1').getAttribute("src") == "videos/2/2.png"){       
        document.getElementById('image1').src = "videos/3/3.png";
        document.getElementById('image2').src = "videos/1/1.png";
        document.getElementById('image3').src = "videos/2/2.png";
        callVideo('1');
        return;
        
    }else{ 
        document.getElementById('image1').src = "videos/1/1.png";
        document.getElementById('image2').src = "videos/2/2.png";
        document.getElementById('image3').src = "videos/3/3.png";
        callVideo('2');
        return;
    }
}

function callVideo(id){
    
    var req = new XMLHttpRequest();
    req.onreadystatechange = function(){
        if(req.readyState == 4){
            document.getElementById('videocontent').innerHTML = req.responseText;
        }
    };
    req.open("GET", "videos/" + id + "/" + id + ".html", true);
    req.send();
}


function videoDesc(){
    if(document.getElementById('videodesc').style.display == "none"){
        document.getElementById('videodesc').style.display = "block";
    }else{
        document.getElementById('videodesc').style.display = "none";
    }
}

function searchVideo(){
    var id = document.getElementById('search').value;
    callVideo(id);
}